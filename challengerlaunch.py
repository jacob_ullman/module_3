import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import cross_validate
from sklearn.linear_model import RidgeClassifier, LogisticRegression
from sklearn.pipeline import Pipeline
from interpret import show
from interpret.data import Marginal

data = pd.read_csv('/content/drive/MyDrive/Colab Notebooks/challenger-data.xls').drop('Observation', axis=1)
scaler = StandardScaler()
X = scaler.fit_transform(X=data[['X']])
y = data['Y']
# We'll start by visualizing the margainl distribution between X and Y
marginals = Marginal()
show(marginals.explain_data(X=data[['X']], y=data['Y']))

'''We end up using simple Logistic Regression and the ridge varient in fitting this data. 
After plotting both, it is obvious that launch at 30 degrees is a terrible idea. 
A property of scikit-learn is that we can only get probabilistic predictions from the LogsisticRegression 
and not the RidgeClassifier class, but using that I can tell you that it was a rougly 80% chance that 
Challenger explode when launching in these conditions.'''

ridge = RidgeClassifier(alpha=0.1, normalize=True)
cross_validate(estimator=ridge, X=X, y=y, n_jobs=12, return_train_score=True)

ridge.fit(X=X, y=y)
ridge.coef_, ridge.intercept_

def sigmoid(x, model):
    return 1 / (1 + np.exp(-(model.intercept_ + model.coef_*x)))

rng = np.arange(-6, 3, .1)
pred = sigmoid(rng, ridge).reshape(-1)
plt.scatter(X, y)
plt.plot(rng, pred)

plt.show()

linear = LogisticRegression()
linear.fit(X, y)
def predict_risk(temp):
    on_the_day = scaler.transform(X=[[temp]])
    return linear.predict_proba(on_the_day)[:, 1]
  
print(predict_risk(30))

rng = np.arange(-6, 3, .1)
pred = sigmoid(rng, linear).reshape(-1)
plt.scatter(X, y)
plt.plot(rng, pred)
plt.show()
